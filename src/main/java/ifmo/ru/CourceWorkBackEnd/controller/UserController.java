package ifmo.ru.CourceWorkBackEnd.controller;

import ifmo.ru.CourceWorkBackEnd.DTO.AuthResponse;
import ifmo.ru.CourceWorkBackEnd.DTO.UserDTO;
import ifmo.ru.CourceWorkBackEnd.filter.JwtProvider;
import ifmo.ru.CourceWorkBackEnd.model.User;
import ifmo.ru.CourceWorkBackEnd.service.auto.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

@RestController
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private JwtProvider jwtProvider;

    @PostMapping("/register")
    public ResponseEntity<String> registerUser(@RequestBody @Valid UserDTO userDTO) {
        logger.info("Register request for user: {}", userDTO.getLogin()); // Логируем логин пользователя
        if (userService.findByLogin(userDTO.getLogin()) != null) {
            return new ResponseEntity<>("Такой пользователь уже есть!", HttpStatus.CONFLICT);
        }
        User u = new User();
        System.out.println(u);
        u.setPassword(userDTO.getPassword());
        u.setLogin(userDTO.getLogin());
        userService.saveUser(u, userDTO.getRole());
        logger.info("Register successful for user: {}", userDTO.getLogin()); // Логируем успешный логин
        return new ResponseEntity<>("Ok", HttpStatus.OK);
    }

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @PostMapping("/login")
    public ResponseEntity<String> auth(@RequestBody UserDTO request) {
        long startTime = System.currentTimeMillis(); // Записываем время начала запроса
        try {
            logger.info("Login request for user: {}", request.getLogin()); // Логируем логин пользователя
            User userEntity = userService.findByLoginAndPassword(request.getLogin(), request.getPassword());
            String token = jwtProvider.generateToken(userEntity.getLogin());
            logger.info("Login successful for user: {}", request.getLogin()); // Логируем успешный логин
            return new ResponseEntity<>(token, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Login failed for user: {}", request.getLogin(), e.getMessage()); // Логируем ошибку логина
            return new ResponseEntity<>("Неверный логин или пароль", HttpStatus.UNAUTHORIZED);
        } finally {
            long endTime = System.currentTimeMillis(); // Записываем время окончания запроса
            logger.info("Request duration: {} ms", (endTime - startTime)); // Логируем продолжительность запроса
        }
    }
}
