package ifmo.ru.CourceWorkBackEnd.service.auto;

import ifmo.ru.CourceWorkBackEnd.model.Role;
import ifmo.ru.CourceWorkBackEnd.model.User;
import ifmo.ru.CourceWorkBackEnd.repository.RoleRepository;
import ifmo.ru.CourceWorkBackEnd.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userEntityRepository;
    @Autowired
    private RoleRepository roleEntityRepository;

    public User saveUser(User userEntity, String role) {
        userEntity.setPassword(userEntity.getPassword());
        return userEntityRepository.save(userEntity);
    }

    public User findByLogin(String login) {
        return userEntityRepository.findByLogin(login);
    }

    public User findByLoginAndPassword(String login, String password) {
        User userEntity = findByLogin(login);
        if (userEntity != null) {
            if (password.equals( userEntity.getPassword())) {
                return userEntity;
            }
        }
        return null;
    }
}
