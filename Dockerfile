FROM maven:3.9.2 AS builder
COPY . .
RUN mvn clean package

FROM openjdk:8-jre-slim
ARG JAR_FILE=/target/*.jar
COPY --from=builder ${JAR_FILE} app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]
